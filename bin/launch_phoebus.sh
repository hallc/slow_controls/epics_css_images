#!/bin/bash

singularity run --app phoebus \
   -B /mnt/old_system:/mnt/old_system  -B /mnt/old_home:/mnt/old_home \
   Singularity.alarm_server.simg   -settings settings.ini 
