#!/bin/sh

if [ -z "$KAFKA_SERVER_CONFIG_DIR" ]
then
      echo "\$KAFKA_SERVER_CONFIG_DIR is empty"
      export KAFKA_SERVER_CONFIG_DIR=/opt/kafka/config
else
      echo "\$KAFKA_SERVER_CONFIG_DIR is NOT empty"
fi

/opt/kafka/bin/zookeeper-server-start.sh ${KAFKA_SERVER_CONFIG_DIR}/zookeeper.properties
