#!/bin/bash

if [ -z "$KAFKA_SERVER_CONFIG_DIR" ]
then
      echo "\$KAFKA_SERVER_CONFIG_DIR is empty"
      export KAFKA_SERVER_CONFIG_DIR=/opt/kafka/config
else
      echo "\$KAFKA_SERVER_CONFIG_DIR is NOT empty"
fi

#cd /opt/kafka

/opt/kafka/bin/kafka-server-start.sh ${KAFKA_SERVER_CONFIG_DIR}/server.properties

