# EPICS Alarm Server 

## Introduction

Contains the Docker and singularity files need to build a container containing 
Control System Studies (phoebus version) and run a kafka server needed to run 
the alarm-server. 


## Quick start


Download the singularity image:
```
wget \
https://eicweb.phy.anl.gov/whit/epics_css_image/-/jobs/artifacts/master/raw/build/Singularity.alarm_server.simg?job=alarm_server_singularity\
 -O Singularity.alarm_server.simg

wget \
https://eicweb.phy.anl.gov/whit/epics_css_image/-/jobs/artifacts/master/raw/build/Singularity.css_phoebus.simg?job=css_phoebus_singularity\
 -O Singularity.css_phoebus.simg
```


```
# zookeeper
singularity run --app zookeeper -B /group:/group -B /work:/work Singularity.alarm_server.simg

# kafka
singularity run --app kafka -B /group:/group -B /work:/work Singularity.alarm_server.simg

# alarm-server
singularity run --app alarm-server -B /group:/group -B /work:/work Singularity.alarm_server.simg
```

```
singularity exec \
   -B /mnt/old_system:/mnt/old_system  -B /mnt/old_home:/mnt/old_home \
   alarm_server.simg   \
   /opt/css/phoebus-0.0.1/phoebus.sh -settings alarm_preferences.properties
```


## Software 

https://github.com/shroffk/phoebus


Create the topics

```
singularity exec \
   -B /mnt/old_system:/mnt/old_system  -B /mnt/old_home:/mnt/old_home \
   alarm_server.simg   \
/opt/bin/create_alarm_topics.sh Scandalizer ```
