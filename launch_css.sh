#!/bin/bash

export KAFKA_SERVER_CONFIG_DIR=/group/c-csv/whit/software/epics/apps/css_alarm_server/config

singularity apps ${HALLC_IMAGES}/Singularity.alarm_server.simg

#singularity shell  ${HALLC_IMAGES}/Singularity.alarm_server.simg

#singularity exec \
#  -B /net:/net  -B /group:/group -B /work:/work -B  /volatile:/volatile \
#   ${HALLC_IMAGES}/Singularity.alarm_server.simg   \
#   /opt/css/phoebus-0.0.1/phoebus.sh -settings alarm_preferences.properties

singularity run --app phoebus  \
  -B /net:/net  -B /group:/group \
  -B /work:/work -B  /volatile:/volatile \
  ${HALLC_IMAGES}/Singularity.alarm_server.simg 

